Progetto di Data Mining - Consumi Elettrici
===

*Tommaso Perini* e *Shoichi Yip*

In questo progetto abbiamo utilizzato le tecniche di Data Mining apprese durante
il corso per studiare i dati del dataset di [Barlacchi et al.](https://www.nature.com/articles/sdata201555).
Per determinare alcune proprietà interessati del dataset in EDA ci siamo avvalsi
dell'uso di dati dell'ISPAT e dell'ISTAT, poiché i dati amministrativi contenuti
nel dataset suddetto sono datati e non contegono informazioni sul grado di
industrializzazione di un luogo.

Abbiamo attinto al [Harvard Dataverse](https://dataverse.harvard.edu/dataverse/bigdatachallenge)
per ottenere i file raw del dataset.

In queste cartelle, per il momento, si trovano:

- la **Exploratory Data Analysis** e la **preparazione dei dati** si trovano nel notebook
  [Preparazione e Visualizzazione](https://shoyip.gitlab.io/setml/Consumi%20Elettrici%20-%20Preparazione%20e%20Visualizzazione.html)
  [[file ipynb](notebooks/Consumi\ Elettrici\ -\ Preparazione\ e\ Visualizzazione.ipynb)]
- la **Regressione** si trova nel notebook
  [Regresssione](https://shoyip.gitlab.io/setml/Consumi%20Elettrici%20-%20Regressione.html)
  [[file ipynb](notebooks/Consumi\ Elettrici\ -\ Regressione.ipynb)]
- la **Classificazione** si trova nel notebook
  [Classificazione](https://shoyip.gitlab.io/setml/Consumi%20Elettrici%20-%20Classificazione.html)
  [[file ipynb](notebooks/Consumi\ Elettrici\ -\ Classificazione.ipynb)]

## Preparazione, Training e Test

Per preparare i dati, allenare il modello e testarlo, utilizzare i seguenti comandi:

- `make data` per installare dipendenze mancanti e preparare i dati processati a
    partire dai dati raw;
- `make train_regressor_pat` per allenare il modello del regressore sui dati PAT
    dei consumi elettrici;
- `make test_regressor_pat` per testare il modello del regressore sui dati PAT
    dei consumi elettrici;
- `make train_regressor_tn` per allenare il modello del regressore sui dati del
    Comune di Trento dei consumi elettrici;
- `make test_regressor_tn` per testare il modello del regressore sui dati del
    Comune di Trento dei consumi elettrici;
