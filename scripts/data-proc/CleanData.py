#!/usr/bin/env python

# Importiamo le librerie necessarie per la preparazione dati
import pandas as pd
pd.options.mode.chained_assignment = None
import geopandas as gpd
import json
from shapely.geometry import Point, shape, Polygon, MultiPolygon, MultiPoint
from pathlib import Path
import datetime
import pickle
import numpy as np
import os.path
from shapely.ops import nearest_points
raw_path = Path('./../../data/raw')
proc_path = Path('./../../data/processed')
from CleanDataClass import *

# Importiamo i dati in DataFrames e definiamo il DataFrame con tutti i consumi elettrici
set1, set2, lines, precip, precip_avail, meteo, grid, tweets, regions, lu, luw = load_data(raw_path)
set_tot = pd.concat([set1, set2], ignore_index=True)

# Impostiamo il GeoDataFrame che contiene i consumi totali della Provincia
# Sarà utile poi per la visualizzazione
consgrid = get_cons_grid(set_tot, lines, grid)
consgrid['geometry'] = consgrid['geometry'].centroid

# Creiamo un DataFrame con i dati aggregati per le fasce orarie 08-19 e 19-24
set_data = define_set_data(set_tot)
set_data.columns = ['LINESET', 'date', 'early_cons', 'late_cons']
lines1 = lines.copy().drop(columns='cons')

# Definiamo un DataFrame con i dati dei consumi aggregati
# per fasce, per ogni data per ogni cella geografica
setgrid_datecell = define_sgdc(set_data, lines, grid)

with open('./../../data/raw/tn_adm.json') as f:
    adm_json = json.load(f)
for d in adm_json['features']:
    d['geometry'] = shape(d['geometry'])
adm_gdf = gpd.GeoDataFrame(adm_json['features'])
adm = adm_gdf.from_features(adm_json['features'])
adm_red = adm[['geometry', 'name', 'com_catasto_code', 'com_istat_code']] # dataframe ridotto

# Importiamo i dati della popolazione residente per comune
population = pd.read_csv('./../../data/raw/tavola_pop_res01.csv')
population.columns = ['com_code', 'municipalities', 'm_pop', 'f_pop', 'tot_pop']
population['com_code'] = population['com_code'].apply(lambda x: int(x))

# Ci sono ancora alcuni dei comuni non accorpati (presumibilmente pre-2020)
# Carichiamo la tabella dal sito dell'ISPAT, correggiamo gli errori ortografici
# (ahimè...) e accorpiamo i dati dei comuni soppressi tra il 2017 ed il 2020
with open('./../../data/raw/TavA25_2016.htm', 'r', encoding='ISO-8859-1') as file:
    localu = file.read().replace('\n', '')
localudf = correct_localudf(localu)

# Associamo i dati dell'ISPAT a quelli dei consumi
lucons = get_lucons(adm_red, population, localudf, consgrid)

# Semplifichiamo il dataset Twitter e associamo i conteggi alle celle geografiche
tweetgrid_datecell = get_tgdc(tweets, grid)

# Semplifichiamo il dataset meteo e associamo i dati meteorologici alle celle geografiche
meteogrid_datecell = get_mgdc(meteo, grid)

# Trovo le celle specifiche del Comune di Trento da usare da filtro per la seconda
# parte della regressione e per la classificazione
adm_red.crs = 'epsg:4326'
tncells = gpd.sjoin(grid, adm_red[adm_red['name'] == 'Trento'], op='intersects')[['cellId']]
tncells_list = (tncells.values.T.tolist()[0])

# Prepariamo i DataFrame da usare nella regressione:
# per ogni raggruppamento per data delle celle si traspongono
# le celle e si creano nuove colonne per ogni cella geografica
# per le due fasce orarie (quindi possiamo usarle come features)
sgdc1, sgdc2 = get_sgdc(setgrid_datecell, tncells_list)

# Prepariamo i DataFrame che useremo nella classificazione
total_datecell = get_total_dc(meteogrid_datecell, tweetgrid_datecell)

# Prepariamo i DataFrame che useremo nella regressione
tot_df, total_datecell = get_tot_df(total_datecell, sgdc1)
tn_df = get_tn_df(total_datecell, tncells_list, sgdc2)

# Esportiamo i datasets che useremo per regressione e clasificazione
outfile = open('./../../data/processed/total_datecell0.pkl', 'wb')
pickle.dump(total_datecell, outfile)
outfile.close()
outfile = open('./../../data/processed/tot_df.pkl', 'wb')
pickle.dump(tot_df, outfile)
outfile.close()
outfile = open('./../../data/processed/tn_df.pkl', 'wb')
pickle.dump(tn_df, outfile)
outfile.close()