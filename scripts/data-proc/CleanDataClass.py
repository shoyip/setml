# Importiamo le librerie necessarie per la preparazione dati
import pandas as pd
pd.options.mode.chained_assignment = None
import geopandas as gpd
import json
from shapely.geometry import Point, shape, Polygon, MultiPolygon, MultiPoint
from pathlib import Path
import datetime
import pickle
import numpy as np
import os.path
from shapely.ops import nearest_points
raw_path = Path('./../../data/raw')
proc_path = Path('./../../data/processed')
from CleanDataClass import *

def load_data(path):
    files = {'grid':'trentino-grid.geojson',
             'adm_reg':'administrative_regions_Trentino.json',
            'weather':'meteotrentino-weather-station-data.json',
            'precip':'precipitation-trentino.csv',
            'precip-avail':'precipitation-trentino-data-availability.csv',
            'SET-1':'SET-nov-2013.csv',
            'SET-2':'SET-dec-2013.csv',
            'SET-lines':'line.csv',
            'twitter':'social-pulse-trentino.geojson'}
    set1 = pd.read_csv(path / files['SET-1'], names = ['LINESET', 'TIMESTAMP', 'POWER'])
    set2 = pd.read_csv(path / files['SET-2'], names = ['LINESET', 'TIMESTAMP', 'POWER'])
    lines = pd.read_csv(path / files['SET-lines'])
    precip = pd.read_csv(path / files['precip'], names = ['TIMESTAMP','CELL_ID', 'INTENSITA'])
    precip_avail = pd.read_csv(path / files['precip-avail'], names = ['TIMESTAMP', 'AFFIDABILE'])
    with open(path / files['weather']) as f:
        meteo_json = json.load(f)
    meteo = gpd.GeoDataFrame(meteo_json['features'])
    meteo['geometry'] = meteo['geomPoint.geom'].apply(lambda x: Point(x['coordinates'][0], x['coordinates'][1]))
    meteo.drop(columns=['geomPoint.geom'], inplace=True)
    grid = gpd.read_file(path / files['grid'])
    with open(path / files['twitter']) as f:
        tweets_json = json.load(f)
    tweets = gpd.GeoDataFrame(tweets_json['features'])
    tweets['geometry'] = tweets['geomPoint.geom'].apply(lambda x: Point(x['coordinates'][0], x['coordinates'][1]))
    tweets.drop(columns=['geomPoint.geom'], inplace=True)
    with open(path / files['adm_reg']) as f:
        regions_json = json.load(f)
    regions = gpd.GeoDataFrame(regions_json['items'])
    localunits = pd.read_csv(path / 'localunits.csv')
    localunitworkers = pd.read_csv(path / 'localunitworkers.csv')
    return set1, set2, lines, precip, precip_avail, meteo, grid, tweets, regions, localunits, localunitworkers

def findnorm(df1, df2, row):
    return df1['NR_UBICAZIONI'][row] / df2.loc[df1['LINESET'][row]]

def get_cons_grid(setdf, linesdf, gridgdf):
    month_cons = setdf.groupby(['LINESET']).sum()
    # Troviamo il numero totale di utenze per linea
    lineuser = linesdf.groupby(['LINESET']).sum()['NR_UBICAZIONI']
    # Troviamo il fattore di normalizzazione
    linesdf['norm'] = [findnorm(linesdf, lineuser, row) for row in linesdf['NR_UBICAZIONI']]
    def findcons(df1, df2, row):
        try:
            result = df1['norm'][row] * df2.loc[df1['LINESET'][row]][0]
        except:
            result = None
        return result
    linesdf['cons'] = [findcons(linesdf, month_cons, row) for row in linesdf['NR_UBICAZIONI']]
    cellcons = linesdf.groupby(['SQUAREID'])[['cons']].sum()
    consgrid = pd.merge(gridgdf, cellcons, left_on='cellId', right_on='SQUAREID', how='left')
    return consgrid

def define_set_data(set_tot):
    if os.path.exists('./../../data/interim/set_data.pkl'):
        infile = open('./../../data/interim/set_data.pkl', 'rb')
        set_data=pickle.load(infile)
        infile.close()
    else:
        print('ATTENZIONE! Il seguente procedimento può impiegare 1-2 ore.')
        print('È in corso un riarrangiamento dei dati in base alle fasce orarie...')
        mylist = []
        for line in set_tot['LINESET'].unique():
            firstday = set_tot['datetime'].iloc[0]
            for day in range((set_tot.iloc[-1]['datetime'] - set_tot.iloc[0]['datetime']).days+1):
                meancons1 = set_tot[set_tot['LINESET'] == line].iloc[day*144+48 : day*144+114].mean()[0]
                meancons2 = set_tot[set_tot['LINESET'] == line].iloc[day*144+114 : (day+1)*144].mean()[0]
                mylist.append([line, firstday+datetime.timedelta(days=day), meancons1, meancons2])
        df4 = pd.DataFrame(mylist)
        outfile = open('./../../data/interim/set_data.pkl', 'wb')
        pickle.dump(df4, outfile)
        outfile.close()
    return set_data

def define_sgdc(set_data, lines, grid):
    if os.path.exists('./../../data/interim/setgrid_datecell.pkl'):
        infile = open('./../../data/interim/setgrid_datecell.pkl', 'rb')
        setgrid_datecell=pickle.load(infile)
        infile.close()
    else:
        setcons_date = pd.merge(set_data, lines, how='left', on='LINESET')
        df5_1 = setcons_date.groupby(['date', 'LINESET']).apply(lambda x: (x['norm']*x['early_cons']).sum()).reset_index()
        df5_1.columns = ['date', 'LINESET', 'norm_early_cons']
        df5_2 = setcons_date.groupby(['date', 'LINESET']).apply(lambda x: (x['norm']*x['late_cons']).sum()).reset_index()
        df5_2.columns = ['date', 'LINESET', 'norm_late_cons']
        norm_cons = pd.concat([df5_1, df5_2], axis=1)
        norm_cons = norm_cons.loc[:,~norm_cons.columns.duplicated()]
        setcons_datecell = pd.merge(setcons_date, norm_cons, left_on=['LINESET', 'date'], right_on=['LINESET', 'date']).groupby(['SQUAREID', 'date']).sum().reset_index()
        setcons_datecell.drop(columns=['early_cons', 'late_cons', 'NR_UBICAZIONI', 'norm', 'cons'], inplace=True)
        setgrid_datecell = pd.merge(setcons_datecell, grid, left_on='SQUAREID', right_on='cellId')
        setgrid_datecell.geometry = setgrid_datecell.geometry.apply(lambda x: x.centroid)
        outfile = open('./../../data/interim/setgrid_datecell.pkl', 'wb')
        pickle.dump(setgrid_datecell, outfile)
        outfile.close()
    return setgrid_datecell

def correct_localudf(localu):
    localudf_raw = pd.read_html(localu)
    localudf = localudf_raw[0][['Comuni', 'Industria in senso stretto', 'Costruzioni', 'Commercio, trasporti e pubblici esercizi', 'Altri servizi', 'Totale']]
    localudf.columns = ['municipalities', 'industry', 'building', 'tertiary', 'other', 'total']
    localudf = localudf.sort_values('municipalities')
    col_list=['industry', 'building', 'tertiary', 'other', 'total']
    localudf.loc[:, col_list] = localudf.loc[:, col_list].replace('-', 0)
    localudf['building'] = localudf['building'].astype('int64')
    localudf['industry'] = localudf['industry'].astype('int64')
    newCityArr = ['Terre d\'Adige']
    oldCitiesArr = [['Zambana', 'Nave San Rocco']]
    for newCity, oldCities in zip(newCityArr, oldCitiesArr):
        dictNewCity = {'municipalities': newCity, **localudf[localudf['municipalities'].isin(oldCities)].sum(numeric_only=True).to_dict()}
        localudf = localudf.append((dictNewCity), ignore_index=True)
    localudf = localudf[~localudf.municipalities.isin([val for sublist in oldCitiesArr for val in sublist])]
    return localudf

def get_lucons(adm_red, population, localudf, consgrid):
    units_municip = pd.merge(adm_red, localudf, left_on='name', right_on='municipalities', how='left')
    units_municip.crs = 'epsg:4326'
    units_municip['com_code'] = units_municip['com_istat_code'].apply(lambda x: int(x))
    units_municip_pop = pd.merge(units_municip, population, how='left', left_on='com_code', right_on='com_code')
    luconsraw_gdf = gpd.sjoin(units_municip_pop, consgrid, how='left')
    luconsdf = pd.merge(localudf, luconsraw_gdf.groupby('name')[['cons', 'tot_pop']].sum(), how='left', left_on='municipalities', right_on='name')
    return luconsdf

def nearest_station(griddf, meteodf):
    nearest_arr = []
    df1 = griddf.copy()
    df2 = meteodf.copy()
    pts = df2.geometry.unary_union
    for idx, row in df1.iterrows():
        pt = row.geometry.centroid
        nearest_arr.append(nearest_points(pt, pts)[1])
    df3 = pd.Series(nearest_arr, name='nearest')
    df4 = pd.concat([df1,df3], axis=1)
    return df4

def get_mgdc(meteo, grid):
    meteo_red = meteo[['station', 'elevation', 'date', 'timestamp', 'minTemperature', 'maxTemperature', 'minWind', 'maxWind', 'geometry']]
    meteo_red_1 = meteo_red.copy()
    meteo_red_1['mean_prec'] = meteo.filter(regex=("precipitations.*")).mean(axis=1)
    meteo_red_1['mean_wind'] = meteo[['minWind', 'maxWind']].mean(axis=1)
    meteo_red_1['mean_temp'] = meteo[['minTemperature', 'maxTemperature']].mean(axis=1)
    meteo_red_1.drop(columns=['minTemperature', 'maxTemperature', 'minWind', 'maxWind'], inplace=True)
    meteo_red_1.crs = 'epsg:4326'
    grid_nrst = nearest_station(grid, meteo).drop(columns='geometry')
    grid_nrst.columns = ['cellId', 'geometry']
    meteogrid_datecell = gpd.sjoin(grid_nrst, meteo_red_1, how='left', op='intersects')
    meteogrid_datecell = meteogrid_datecell[['cellId', 'date', 'mean_prec', 'mean_wind', 'mean_temp']]
    return meteogrid_datecell

def get_tgdc(tweets, grid):
    tweets['datetime'] = tweets['created'].apply(lambda x: datetime.datetime.strptime(x, '%Y-%m-%dT%H:%M:%S'))
    tweets['weekday'] = tweets['datetime'].apply(lambda x: x.weekday())
    tweets['date'] = tweets['datetime'].apply(lambda x: x.date())
    tweets_red = tweets[['municipality.name', 'geometry', 'datetime', 'weekday', 'date']]
    tweets_red['lat'], tweets_red['long'] = tweets['geometry'].x, tweets['geometry'].y
    tweets.crs = 'epsg:4326'
    grid.crs = 'epsg:4326'
    tweetgrid_datecell = gpd.sjoin(tweets, grid, how='left', op='within').groupby(['cellId', 'date']).count()['timestamp'].reset_index()
    tweetgrid_datecell.columns = ['cellId', 'date', 'tweetcount']
    return tweetgrid_datecell

def get_sgdc(setgrid_datecell, tncells_list):
    sgdc1 = setgrid_datecell[['date', 'norm_early_cons', 'norm_late_cons', 'cellId']].copy()
    sgdc1.columns = ['date', 'pca', 'pcb', 'cellId'] # previous consumption a and b
    sgdc1 = sgdc1.pivot(index='date', columns='cellId', values=['pca', 'pcb'])
    sgdc1['pat_cons'] = sgdc1.sum(axis=1)
    sgdc1['tn_cons'] = sgdc1.loc[:, (slice(None), tncells_list)].sum(axis=1)
    sgdc1['pca'] = sgdc1['pca'].shift(periods=1)
    sgdc1['pcb'] = sgdc1['pcb'].shift(periods=1)
    sgdc1.loc['2013-11-01', :] = sgdc1.loc['2013-11-02',:].copy()
    sgdc1.columns = [s1 + str(s2) for (s1,s2) in sgdc1.columns.tolist()]
    sgdc1.reset_index(inplace=True)
    sgdc2 = setgrid_datecell[['date', 'norm_early_cons', 'norm_late_cons', 'cellId']].copy()
    sgdc2.columns = ['date', 'pca', 'pcb', 'cellId'] # previous consumption a and b
    sgdc2 = sgdc2.pivot(index='date', columns='cellId', values=['pca', 'pcb'])
    sgdc2 = sgdc2.loc[:, (slice(None), tncells_list)]
    sgdc2['tn_cons'] = sgdc2.sum(axis=1)
    sgdc2['pca'] = sgdc2['pca'].shift(periods=1)
    sgdc2['pcb'] = sgdc2['pcb'].shift(periods=1)
    sgdc2.loc['2013-11-01', :] = sgdc2.loc['2013-11-02',:].copy()
    sgdc2.columns = [s1 + str(s2) for (s1,s2) in sgdc2.columns.tolist()]
    sgdc2.reset_index(inplace=True)
    return sgdc1, sgdc2

def get_total_dc(meteogrid_datecell, tweetgrid_datecell):
    meteogrid_datecell['date'] = meteogrid_datecell['date'].apply(lambda x: datetime.datetime.strptime(str(x), '%Y-%m-%d'))
    tweetgrid_datecell['date'] = tweetgrid_datecell['date'].apply(lambda x: datetime.datetime.strptime(str(x), '%Y-%m-%d'))
    total_datecell = pd.merge(tweetgrid_datecell, meteogrid_datecell, how='outer', left_on=['cellId', 'date'], right_on=['cellId', 'date'])
    total_datecell = total_datecell[['date', 'cellId', 'tweetcount', 'mean_prec', 'mean_wind', 'mean_temp']]
    return total_datecell

def get_tot_df(total_datecell, sgdc1):
    tdc1 = total_datecell.groupby('date').sum().reset_index()[['date', 'tweetcount', 'mean_prec', 'mean_wind', 'mean_temp']]
    tot_df = pd.concat([tdc1, sgdc1], axis=1)
    tot_df = tot_df.loc[1:,~tot_df.columns.duplicated()]
    tot_df['weekday'] = tot_df['date'].apply(lambda x: x.weekday())
    return tot_df, total_datecell

def get_tn_df(total_datecell, tncells_list, sgdc2):
    tdc2 = total_datecell[(total_datecell['cellId'].isin(tncells_list))].groupby('date').sum().reset_index()[['date', 'tweetcount', 'mean_prec', 'mean_wind', 'mean_temp']]
    tn_df = pd.concat([tdc2, sgdc2], axis=1)
    tn_df = tn_df.loc[1:,~tn_df.columns.duplicated()]
    tn_df['weekday'] = tn_df['date'].apply(lambda x: x.weekday())
    return tn_df