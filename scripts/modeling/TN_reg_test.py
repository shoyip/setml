#!/usr/bin/env python
    
import pandas as pd
import numpy as np
import pickle
from sklearn.decomposition import PCA
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.metrics import mean_squared_error
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.preprocessing import StandardScaler
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import LabelBinarizer

class LabelBinarizerPipelineFriendly(LabelBinarizer):
    def fit(self, X, y=None):
        """this would allow us to fit the model based on the X input."""
        super(LabelBinarizerPipelineFriendly, self).fit(X)
    def transform(self, X, y=None):
        return super(LabelBinarizerPipelineFriendly, self).transform(X)
    def fit_transform(self, X, y=None):
        return super(LabelBinarizerPipelineFriendly, self).fit(X).transform(X)

class DataFrameSelector(BaseEstimator, TransformerMixin):
    def __init__(self, attribute_names):
        self.attribute_names = attribute_names
    def fit(self, X, y=None):
        return self;
    def transform(self, X):
        return X[self.attribute_names].values

infile = open('../../data/processed/tot_df.pkl','rb')
dfA = pickle.load(infile)
infile.close()
infile = open('../../data/processed/tn_df.pkl','rb')
dfB = pickle.load(infile)
infile.close()

from sklearn.model_selection import train_test_split
trainB, testB = train_test_split(dfB, test_size=0.2, shuffle=False)

tn = trainB.drop(['tn_cons'], axis=1)
tn_labels = trainB['tn_cons'].copy()
tn_num = tn.drop(['weekday', 'date'], axis=1)
tn_cat = tn['weekday']
num_attribs = list(tn_num)
cat_attribs = ['weekday']

X_train = tn
y_train = tn_labels
X_test = testB
y_test = testB['tn_cons'].copy()

infile = open('../../models/TNregressor.pkl', 'rb')
pipe = pickle.load(infile)
infile.close()

print('Test completato!')
score = pipe.score(X_test, y_test)
print("Test score: {0:.2f} %".format(100 * score))
y_predict = pipe.predict(X_test)
print('Valori di consumo predetti:\n', y_predict)
print('Valori di consumo reali:\n', y_test.values)
final_mse = mean_squared_error(y_test.values, y_predict)
final_rmse = np.sqrt(final_mse)
print('Il RMSE è:', final_rmse)