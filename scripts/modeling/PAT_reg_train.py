#!/usr/bin/env python

import pandas as pd
import numpy as np
import pickle

infile = open('../../data/processed/tot_df.pkl','rb')
dfA = pickle.load(infile)
infile.close()
infile = open('../../data/processed/tn_df.pkl','rb')
dfB = pickle.load(infile)
infile.close()

from sklearn.model_selection import train_test_split
trainA, testA = train_test_split(dfA, test_size=0.2, shuffle=False)

pat = trainA.drop(['pat_cons', 'tn_cons'], axis=1)
pat_labels = trainA['pat_cons'].copy()
pat_num = pat.drop(['weekday', 'date'], axis=1, errors='ignore')
pat_cat = pat['weekday']

from sklearn.decomposition import PCA
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.metrics import mean_squared_error
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.preprocessing import StandardScaler
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import LabelBinarizer

class LabelBinarizerPipelineFriendly(LabelBinarizer):
    def fit(self, X, y=None):
        """this would allow us to fit the model based on the X input."""
        super(LabelBinarizerPipelineFriendly, self).fit(X)
    def transform(self, X, y=None):
        return super(LabelBinarizerPipelineFriendly, self).transform(X)
    def fit_transform(self, X, y=None):
        return super(LabelBinarizerPipelineFriendly, self).fit(X).transform(X)

class DataFrameSelector(BaseEstimator, TransformerMixin):
    def __init__(self, attribute_names):
        self.attribute_names = attribute_names
    def fit(self, X, y=None):
        return self;
    def transform(self, X):
        return X[self.attribute_names].values
    
num_attribs = list(pat_num)
cat_attribs = ['weekday']

pca = PCA(whiten=True)
etr = ExtraTreesRegressor(criterion='mae')

num_pipeline = Pipeline([
    ('selector', DataFrameSelector(num_attribs)),
    ('std_scaler', StandardScaler()),
    ('dim_red', pca),
])

cat_pipeline = Pipeline([
    ('selector', DataFrameSelector(cat_attribs)),
    ('label_binarizer', LabelBinarizerPipelineFriendly()),
])

prepare_pipeline = FeatureUnion(transformer_list=[
    ('num_pipeline', num_pipeline),
    ('cat_pipeline', cat_pipeline),
])

full_pipeline = Pipeline([
    ('prepare_pipeline', prepare_pipeline),
    ('regressor', etr),
])

X_train = pat
y_train = pat_labels
X_test = testA
y_test = testA['pat_cons'].copy()

pipe = full_pipeline.fit(X_train, y_train)
print('Training completato sul modello ExtraTreeRegressor!')
outfile = open('../../models/PATregressor.pkl', 'wb')
pickle.dump(pipe, outfile)
outfile.close()