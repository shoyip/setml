import pandas as pd
import numpy as np

setdf = pd.read_csv('data/SET-nov-2013.csv', names = ['LINESET', 'TIMESTAMP', 'POWER'])
lines = pd.read_csv('data/line.csv')

def findnorm(df1, df2, row):
    return df1['NR_UBICAZIONI'][row] / df2.loc[df1['LINESET'][row]]
# Troviamo il numero totale di utenze per linea
lineuser = lines.groupby(['LINESET']).sum()['NR_UBICAZIONI']
# Troviamo il fattore di normalizzazione
lines['norm'] = [findnorm(lines, lineuser, row) for row in lines['NR_UBICAZIONI']]

tsdf = setdf.groupby('LINESET')['POWER'].apply(lambda x: np.array(x)).reset_index()
# Ci sono delle time series con qualche elemento di troppo, rimuoviamoli in modo che possiamo confrontare le serie tra loro
for index, row in tsdf.iterrows():
    tsl = len(tsdf['POWER'][index])
    if (tsl != 4318):
        timecorrection = tsl-4318
        tsdf.iloc[index]['POWER'] = (tsdf.iloc[index]['POWER'][:-timecorrection])
tsdf_lines = pd.merge(lines, tsdf, left_on='LINESET', right_on='LINESET', how='left')
# Definiamo le time series "pesate" per l'uso nella cella e sommiamo le serie per tutte le linee presenti sulla cella
tsdf_lines['wp'] = tsdf_lines['POWER']*tsdf_lines['norm']
tsdf1_cells = tsdf_lines.groupby('SQUAREID').wp.apply(np.sum)
print(tsdf1_cells)
