def getTSdf(setdf, linesdf):
    tsdf = setdf.groupby('LINESET')['POWER'].apply(lambda x: np.array(x)).reset_index()
    # Ci sono delle time series con qualche elemento di troppo, rimuoviamoli in modo che possiamo confrontare le serie tra loro
    for index, row in tsdf.iterrows():
        tsl = len(tsdf['POWER'][index])
        if (tsl != 4318):
            timecorrection = tsl-4318
            tsdf.iloc[index]['POWER'] = (tsdf.iloc[index]['POWER'][:-timecorrection])
    tsdf_lines = pd.merge(linesdf, tsdf, left_on='LINESET', right_on='LINESET', how='left')
    # Definiamo le time series "pesate" per l'uso nella cella e sommiamo le serie per tutte le linee presenti sulla cella
    tsdf_lines['wp'] = tsdf_lines['POWER']*tsdf_lines['norm']
    tsdf_cells = tsdf_lines.groupby('SQUAREID').wp.apply(np.sum)
    df1 = tsdf_cells.reset_index()
    df1['wp'] = [np.zeros(4318) if np.any(x == 0) else x for x in df1.wp]
    df3 = pd.DataFrame(np.stack(df1.wp.values))
    resultdf = pd.concat([df1, df3], axis=1)
    return resultdf.drop('wp', axis=1)

df = getTSdf(set_tot, lines)