import pandas as pd

with open('data/TavA25_2016.htm', 'r', encoding='ISO-8859-1') as file:
    localu = file.read().replace('\n', '')
localudf_raw = pd.read_html(localu)
localudf = localudf_raw[0][['Comuni', 'Industria in senso stretto', 'Costruzioni', 'Commercio, trasporti e pubblici esercizi', 'Altri servizi', 'Totale']]
localudf.columns = ['municipalities', 'industry', 'building', 'tertiary', 'other', 'total']
localudf = localudf.sort_values('municipalities')
col_list=['industry', 'building', 'tertiary', 'other', 'total']
localudf.loc[:, col_list] = localudf.loc[:, col_list].replace('-', 0)
#localudf['building'] = pd.to_numeric(localudf['building'])
#localudf['industry'] = pd.to_numeric(localudf['industry'])
localudf['building'] = localudf['building'].astype('int64')
localudf['industry'] = localudf['industry'].astype('int64')
newCityArr = ['Novella', 'Borgo d\'Anaunia', 'Terre d\'Adige', 'Ville di Fiemme']
oldCitiesArr = [['Brez', 'Cagnò', 'Cloz', 'Revò', 'Romallo'], ['Castelfondo', 'Fondo', 'Malosco'], ['Zambana', 'Nave San Rocco'], ['Carano', 'Daiano', 'Varena']]
for newCity, oldCities in zip(newCityArr, oldCitiesArr):
    dictNewCity = {'municipalities': newCity, **localudf[localudf['municipalities'].isin(oldCities)].sum(numeric_only=True).to_dict()}
    localudf = localudf.append((dictNewCity), ignore_index=True)
localudf = localudf[~localudf.municipalities.isin([val for sublist in oldCitiesArr for val in sublist])]
localudf.to_csv('data/localunits.csv', index=False)

with open('data/TavA26_2016.htm', 'r', encoding='ISO-8859-1') as file:
    localuw = file.read().replace('\n', '')
localuwdf_raw = pd.read_html(localuw)
localuwdf = localuwdf_raw[0][['Codice comune', 'Comuni', 'Industria in senso stretto', 'Costruzioni', 'Commercio, trasporti e pubblici esercizi', 'Altri servizi', 'Totale']]
localuwdf.columns = ['id', 'municipalities', 'industry', 'building', 'tertiary', 'other', 'total']
localuwdf.drop(columns=['id'], inplace=True)
localuwdf = localuwdf.sort_values('municipalities')
col_list=['industry', 'building', 'tertiary', 'other', 'total']
localuwdf.loc[:, col_list] = localuwdf.loc[:, col_list].replace('-', 0)
localuwdf.fillna(0, inplace=True)
localuwdf['building'] = localuwdf['building'].astype('int64')
localuwdf['industry'] = localuwdf['industry'].astype('int64')
newCityArr = ['Novella', 'Borgo d\'Anaunia', 'Terre d\'Adige', 'Ville di Fiemme']
oldCitiesArr = [['Brez', 'Cagnò', 'Cloz', 'Revò', 'Romallo'], ['Castelfondo', 'Fondo', 'Malosco'], ['Zambana', 'Nave San Rocco', 'Faedo'], ['Carano', 'Daiano', 'Varena']]
for newCity, oldCities in zip(newCityArr, oldCitiesArr):
    dictNewCity = {'municipalities': newCity, **localudf[localudf['municipalities'].isin(oldCities)].sum(numeric_only=True).to_dict()}
    localuwdf = localuwdf.append((dictNewCity), ignore_index=True)
localuwdf = localuwdf[~localuwdf.municipalities.isin([val for sublist in oldCitiesArr for val in sublist])]
localuwdf.to_csv('data/localunitworkers.csv', index=False)
